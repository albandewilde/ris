.PHONY: build run img ctn-run ctn-stop ctn-rm ctn-run-bg desktop-build desktop-run

ifeq ($(shell test -s .env && echo -n yes),yes)
    include .env
	export $(shell sed 's/=.*//' .env)
endif

DOCKER ?= podman

build:
	@go build

run: build
	@./ris

img:
	$(DOCKER) build --no-cache -t ris .

ctn-run:
	@$(DOCKER) run `if [ -f .env ]; then echo "--env-file .env"; fi` --name ris -p $(WEB_PORT):$(WEB_PORT) -p $(METRICS_SERVER_PORT):$(METRICS_SERVER_PORT) -v $(PIC_DIR):$(PIC_DIR) ris

ctn-stop:
	@$(DOCKER) stop ris

ctn-rm:
	@$(DOCKER) container rm ris

ctn-run-bg:
	@$(DOCKER) run `if [ -f .env ]; then echo "--env-file .env"; fi` --name ris -p $(WEB_PORT):$(WEB_PORT) -p $(METRICS_SERVER_PORT):$(METRICS_SERVER_PORT) -v $(PIC_DIR):$(PIC_DIR) -d ris

desktop-build:
	@go build -C ./desktop/ -o ../desktop-app

desktop-run: desktop-build
	@./desktop-app
