package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/albandewilde/ris/discordbot"
	"gitlab.com/albandewilde/ris/metricsserver"
	"gitlab.com/albandewilde/ris/telegrambot"
	"gitlab.com/albandewilde/ris/webserver"
	"golang.org/x/exp/slog"
)

var picDir string // picDir is the directory path where the pictures to send are

var webHost string // webHost is the host address the web server will listen on
var webPort int    // webPort is the port the web server will listen on

var discordTkn string // discordTkn is the discord bot token

var telegramTkn string // telegramTkn is the telegram bot token

var metricsHost string // metricsHost is the host address the web server will listen on
var metricsPort int    // metricsPort is the port the web server will listen on

func init() {
	picDir = os.Getenv("PIC_DIR")
	if picDir == "" {
		slog.Error("the environment variable `PIC_DIR` is empty")
		os.Exit(1)
	}

	webHost = os.Getenv("WEB_HOST")
	if webHost == "" {
		webHost = "0.0.0.0"
	}

	port := os.Getenv("WEB_PORT")
	if port == "" {
		webPort = 8897
	} else {
		p, err := strconv.ParseInt(port, 10, 64)
		if err != nil {
			slog.Error("can't parse the port (environment variable `WEB_PORT`): %w", err)
			os.Exit(1)
		}
		webPort = int(p)
	}

	discordTkn = os.Getenv("DISCORD_TKN")

	telegramTkn = os.Getenv("TELEGRAM_TKN")

	metricsHost = os.Getenv("METRICS_SERVER_HOST")
	if webHost == "" {
		webHost = "0.0.0.0"
	}
	port = os.Getenv("METRICS_SERVER_PORT")
	if port == "" {
		metricsPort = 8080
	} else {
		p, err := strconv.ParseInt(port, 10, 64)
		if err != nil {
			slog.Error("can't parse the port (environment variable `WEB_PORT`): %w", err)
			os.Exit(1)
		}
		metricsPort = int(p)
	}
}

func main() {
	// Prometheus things
	reg := prometheus.NewRegistry() // Create non-global registry.

	// Create new metrics and register them using the custom registry.
	webServerMetrics := webserver.NewMetrics(reg)
	// Start the web server
	srv := webserver.New(webHost, webPort, picDir, webServerMetrics)

	// Create new metrics and register them using the custom registry.
	discordBotMetrics := discordbot.NewMetrics(reg)
	// Start the discord discordBot
	discordBot, err := discordbot.New(discordTkn, picDir, discordBotMetrics)
	if err != nil {
		slog.Error(err.Error())
	}

	// Create new metrics and register them using the custom registry.
	telegramBotMetrics := telegrambot.NewMetrics(reg)
	// Start the telegram bot
	telegramBot, err := telegrambot.New(telegramTkn, picDir, telegramBotMetrics)
	if err != nil {
		slog.Error(err.Error())
	}

	// Prometheus server
	// Expose metrics and custom registry via an HTTP server
	metricsServer := metricsserver.New(metricsHost, metricsPort, reg)

	// Wait an event to close all bots, apps, and services, ...
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Gracefully close the discord bot and the web server
	err = discordbot.Close(discordBot)
	if err != nil {
		slog.Error(err.Error())
	}

	// Ignore the ErrServerClosed because we may have close it
	if err := srv.Shutdown(context.Background()); err != nil && err != http.ErrServerClosed {
		slog.Error(err.Error())
	}

	// Shutdown the telegram bot
	_, err = telegramBot.Close(nil)
	if err != nil {
		slog.Error(err.Error())
	}

	// Close the metrics server and ignore `ErrServerClosed` errors
	if err := metricsServer.Shutdown(context.Background()); err != nil && err != http.ErrServerClosed {
		slog.Error(err.Error())
	}
}
