PIC_DIR=<directory_of_images_to_send>

WEB_HOST=0.0.0.0
WEB_PORT=8897

DISCORD_TKN=<discord_bot_token>

TELEGRAM_TKN=<telegram_bot_token>

PIC_HOST=<host_to_fetch_images>

METRICS_SERVER_HOST=0.0.0.0
METRICS_SERVER_PORT=8080
