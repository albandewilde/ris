package randomfile

import "os"

// GetContent return the content of a random file in the `directoryPath` directory
func GetContent(directoryPath string) ([]byte, error) {
	filePath, err := Get(directoryPath)
	if err != nil {
		return nil, err
	}

	// Read the file
	content, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	return content, nil
}
