package randomfile

import (
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
)

// Get return the path of a random file in the `diretoryPath` directory
func Get(directoryPath string) (string, error) {
	path, err := filepath.Abs(directoryPath)
	if err != nil {
		return "", fmt.Errorf("can't get absolute path of `%s`: %w", directoryPath, err)
	}

	files, err := os.ReadDir(path)
	if err != nil {
		return "", fmt.Errorf("can't read the content of the directory `%s`: %w", path, err)
	}

	id := rand.Intn(len(files))
	file := files[id]

	absFilePath, err := filepath.Abs(directoryPath + file.Name())
	if err != nil {
		return "", fmt.Errorf("can't get absolute path of `%s`: %w", absFilePath, err)
	}

	return absFilePath, nil
}
