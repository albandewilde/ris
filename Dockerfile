FROM golang:1.20 as builder

WORKDIR /usr/src/ris
COPY . .

RUN CGO_ENABLED=0 go build -o /usr/bin/ris


FROM alpine

WORKDIR /bin/ris

COPY --from=builder /usr/bin/ris .

CMD ["./ris"]
