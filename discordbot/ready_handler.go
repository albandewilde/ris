package discordbot

import (
	"log"

	dgo "github.com/bwmarrin/discordgo"
)

func ready(s *dgo.Session, r *dgo.Ready) {
	log.Println("Discord bot ready !")
}
