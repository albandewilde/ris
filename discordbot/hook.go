package discordbot

type Hook interface {
	AddError(err error)      // Called when an error append
	Duration(duration int64) // Called after a successful call with the duration (in miliseconds) to handle the discord command command
}
