package discordbot

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"time"

	dgo "github.com/bwmarrin/discordgo"
	"gitlab.com/albandewilde/ris/randomfile"
)

func getImgHandler(picDir string, hook Hook) func(*dgo.Session, *dgo.InteractionCreate) {
	return func(s *dgo.Session, i *dgo.InteractionCreate) {
		start := time.Now()

		// Chech if the current channel is an nsfw one
		channel, err := s.Channel(i.ChannelID)
		if err != nil {
			log.Println(fmt.Errorf("unable to find the channel to send the message in: %w", err))
			hook.AddError(err)
			return
		}
		if !channel.NSFW {
			err = s.InteractionRespond(i.Interaction, &dgo.InteractionResponse{
				Type: dgo.InteractionResponseChannelMessageWithSource,
				Data: &dgo.InteractionResponseData{
					Content: "Can't send image because we are not in an NSFW channel",
				},
			})
			if err != nil {
				log.Println("can't send message that the current channel is not a NSFW one")
			}
			return
		}

		// Get a random file
		filePath, err := randomfile.Get(picDir)
		if err != nil {
			log.Println(err)
			hook.AddError(err)
		}

		// Read the content of the file
		content, err := os.ReadFile(filePath)
		if err != nil {
			log.Printf("can't read file `%s`: %s\n", filePath, err.Error())
			hook.AddError(err)

			// Tell the user we have problems reading the image
			err = s.InteractionRespond(i.Interaction, &dgo.InteractionResponse{
				Type: dgo.InteractionResponseChannelMessageWithSource,
				Data: &dgo.InteractionResponseData{
					Content: "can't send image now, sorry",
				},
			})
			if err != nil {
				log.Printf("can't send message that we can't read the file: %s\n", err.Error())
				hook.AddError(err)
			}
			return
		}

		// Alert the user we are uploading his image
		err = s.InteractionRespond(i.Interaction, &dgo.InteractionResponse{
			Type: dgo.InteractionResponseDeferredChannelMessageWithSource,
			Data: &dgo.InteractionResponseData{
				Content: "Uploading file...",
			},
		})
		if err != nil {
			log.Printf("Failed sending uploading message: %s\n", err.Error())
			hook.AddError(err)
			return
		}

		// Send the image
		_, err = s.FollowupMessageCreate(i.Interaction, false, &dgo.WebhookParams{
			Files: []*dgo.File{
				{
					Name:   filepath.Base(filePath),
					Reader: bytes.NewReader(content),
				},
			},
		})

		if err != nil {
			log.Printf("can't send file `%s`: %s\n", filePath, err.Error())
			hook.AddError(err)

			// Tell the user we have problems sending the image
			_, err = s.FollowupMessageCreate(i.Interaction, false, &dgo.WebhookParams{
				Content: "can't send image, sorry",
			})
			if err != nil {
				log.Printf("can't send message that we can't send the file: %s\n", err.Error())
				hook.AddError(err)
			}
			return
		}

		hook.Duration(time.Since(start).Milliseconds())
	}
}
