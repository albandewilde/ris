package discordbot

import (
	dgo "github.com/bwmarrin/discordgo"
)

// New create and initialize a new discord bot instance
func New(tkn, picDir string, hook Hook) (*dgo.Session, error) {
	bot, err := dgo.New("Bot " + tkn)
	if err != nil {
		return nil, err
	}

	// Register callback functions
	bot.AddHandler(ready)
	bot.AddHandler(getImgHandler(picDir, hook))

	// Open the bot connection
	err = bot.Open()
	if err != nil {
		return nil, err
	}

	// Register slash commands
	_, err = bot.ApplicationCommandCreate(bot.State.User.ID, "", imgCommand)
	if err != nil {
		return nil, err
	}

	return bot, nil
}

// Close properly close the bot (unregister commands, ...)
func Close(s *dgo.Session) error {
	defer s.Close()

	// Get actual registered slash commands
	commands, err := s.ApplicationCommands(s.State.User.ID, "")
	if err != nil {
		return err
	}

	// Unregister all registered slash commands
	for _, c := range commands {
		e := s.ApplicationCommandDelete(s.State.User.ID, "", c.ID)
		if e != nil {
			err = e
		}
	}

	return err
}

var imgCommand = &dgo.ApplicationCommand{
	Name:        "img",
	Description: "Get a random image",
}
