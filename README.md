# RIS (Random Image Sender)

Choose a random image in a folder and send it.

The bot can be use for many services.
- [x] Discord
- [ ] e-mail
- [x] Web site
- [x] Telegram
- [x] Desktop app
- [ ] Slack
- [ ] gRPC
- [ ] IRC
- [ ] Mobile app
- [ ] [Gemini](https://en.wikipedia.org/wiki/Gemini_(protocol))
- [ ] [Gopher](https://en.wikipedia.org/wiki/Gopher_(protocol))
- [ ] Webex
- [ ] ...

## Start the bot

The project must be started with the Makefile.

### Environment variables

All environments variable must be in the `.env` file. There is an example of what this file must contain
in the `.env.tpl` file.

Here are all environments variables:

| Name | Required | Usage | Example |
|:----:|:---------|:------|:--------|
| `PIC_DIR` | True | This is the path of the directory where the images to send are located | `./img` |
| `WEB_HOST` | False (default: `0.0.0.0`) | The host address the web server will listen on | `localhost` |
| `WEB_PORT` | False (default: `8897`) | The port the web server will listen on | `8090` |
| `DISCORD_TKN` | False (required to have a working discord bot) | The token used by the bot to connect to discord | |
| `TELEGRAM_TKN` | False (required to have a working telegram bot) | The token used by the bot to connect to telegram | |
| `PIC_HOST` | False (required for the desktop app when `PIC_DIR` is unset) | Host the desktop app will use to fetch images | `example.com` |
