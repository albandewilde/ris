package metricsserver

import (
	"fmt"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// New return a new initializes and running server for prometheus metrics
func New(host string, port int, reg *prometheus.Registry) *http.Server {
	srv := http.Server{
		Addr: fmt.Sprintf("%s:%d", host, port),
		Handler: http.Handler(
			promhttp.HandlerFor(reg, promhttp.HandlerOpts{Registry: reg}),
		),
	}

	// Start the server
	log.Printf("Serving metrics on http://%s\n", srv.Addr)
	go srv.ListenAndServe()

	return &srv
}
