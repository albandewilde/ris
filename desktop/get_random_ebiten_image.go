package main

import (
	"fmt"

	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"gitlab.com/albandewilde/ris/randomfile"
)

func getRandomEbitenImageFromFile(directory string) func() (*ebiten.Image, error) {
	return func() (*ebiten.Image, error) {
		// Get a random image path
		path, err := randomfile.Get(directory)
		if err != nil {
			return nil, fmt.Errorf("failed to get a random path: %w", err)
		}

		img, _, err := ebitenutil.NewImageFromFile(path)
		if err != nil {
			return nil, fmt.Errorf("failed to create an ebiten image: %w", err)
		}

		return img, nil
	}
}

func getRandomEbitenImageFromURL(URL string) func() (*ebiten.Image, error) {
	return func() (*ebiten.Image, error) {
		img, err := ebitenutil.NewImageFromURL(URL)
		if err != nil {
			return nil, fmt.Errorf("failed to create an ebiten image: %w", err)
		}

		return img, nil
	}
}

func getRandomEbitenImageFunction(directory, URL string) (func() (*ebiten.Image, error), error) {
	if directory != "" {
		return getRandomEbitenImageFromFile(directory), nil
	}
	if URL != "" {
		return getRandomEbitenImageFromURL(URL), nil
	}

	return nil, fmt.Errorf("failed choosing a function to fetch images: directory or URL must not be empty")
}
