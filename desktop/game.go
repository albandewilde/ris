package main

import (
	"github.com/hajimehoshi/ebiten/v2"
	"github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"golang.org/x/exp/slog"
)

// Game implement ebiten.Game
type Game struct {
	fetch          func() (*ebiten.Image, error) // function called to get an image
	displayedImage *ebiten.Image                 // current image displayed in the window
	message        string                        // error message displayed in the window
}

func newGame(
	fetcher func() (*ebiten.Image, error),
	displayedImage *ebiten.Image,
	message string,

) Game {
	var g Game

	g.fetch = fetcher
	g.displayedImage = displayedImage
	g.message = message

	return g
}

func (g *Game) Update() error {
	if inpututil.IsMouseButtonJustPressed(ebiten.MouseButtonLeft) {
		newRandomImage, err := g.fetch()
		if err != nil {
			slog.Error(err.Error())
			g.message = "Failed getting the image."
			return nil
		}
		g.message = ""
		g.displayedImage = newRandomImage
	}
	return nil
}

func (g *Game) Draw(screen *ebiten.Image) {
	op := &ebiten.DrawImageOptions{}
	screen.DrawImage(g.displayedImage, op)
	ebitenutil.DebugPrint(screen, g.message)
}

func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return g.displayedImage.Bounds().Dx(), g.displayedImage.Bounds().Dy()
}
