package main

import (
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"os"

	_ "golang.org/x/image/webp"

	"github.com/hajimehoshi/ebiten/v2"
)

var (
	fetcher func() (*ebiten.Image, error)
)

func init() {
	dir := os.Getenv("PIC_DIR")
	host := os.Getenv("PIC_HOST")

	var err error
	fetcher, err = getRandomEbitenImageFunction(dir, host)
	if err != nil {
		panic(err)
	}
}

func main() {
	// Window configuration
	ebiten.SetWindowTitle("RIS")
	ebiten.SetWindowResizable(true)
	ebiten.MaximizeWindow()

	initialImage, err := fetcher()
	if err != nil {
		panic(err)
	}

	// Our game initialization
	game := newGame(
		fetcher,
		initialImage,
		"",
	)

	// Start the app
	if err := ebiten.RunGame(&game); err != nil {
		log.Fatal(err)
	}
}
