package telegrambot

import (
	"fmt"
	"time"

	"github.com/PaulSonOfLars/gotgbot/v2"
	"github.com/PaulSonOfLars/gotgbot/v2/ext"
	"gitlab.com/albandewilde/ris/randomfile"
)

// imgHandler return the handler function that send the random image from the directory
func imgHandler(directory string, hook Hook) func(b *gotgbot.Bot, ctx *ext.Context) error {
	return func(b *gotgbot.Bot, ctx *ext.Context) error {
		start := time.Now()

		file, _ := randomfile.GetContent(directory)

		_, err := b.SendPhoto(
			ctx.EffectiveChat.Id,
			file,
			nil,
		)
		if err != nil {
			hook.AddError(err)

			// Tell the user something went wrong sending the image
			errMsgResult := sendErrorMessage(b, ctx)
			if errMsgResult != nil {
				hook.AddError(errMsgResult)
				return fmt.Errorf("can't prevent the user we have failed sending him the image: %w", err)
			}

			return fmt.Errorf("failed to send image: %w", err)
		}

		hook.Duration(time.Since(start).Milliseconds())
		return nil
	}
}

// sendErrorMessage to the user (ignoring all errors that occured)
// We ignore all errors because we assume if we use this function you assyme we are
// in an error recovery case
func sendErrorMessage(b *gotgbot.Bot, ctx *ext.Context) error {
	_, err := ctx.EffectiveMessage.Reply(
		b,
		"An error occured while sending the image, please try again later.",
		&gotgbot.SendMessageOpts{
			ParseMode: "html",
		},
	)

	if err != nil {
		return err
	}

	return nil
}
