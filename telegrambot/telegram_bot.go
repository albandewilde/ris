package telegrambot

import (
	"fmt"
	"net/http"
	"time"

	"github.com/PaulSonOfLars/gotgbot/v2"
	"github.com/PaulSonOfLars/gotgbot/v2/ext"
	"github.com/PaulSonOfLars/gotgbot/v2/ext/handlers"
	"golang.org/x/exp/slog"
)

// New return a started telegram bot
func New(token string, fileDirectory string, hook Hook) (*gotgbot.Bot, error) {
	// Create bot from environment value.
	b, err := gotgbot.NewBot(
		token, &gotgbot.BotOpts{
			Client: *http.DefaultClient,
			DefaultRequestOpts: &gotgbot.RequestOpts{
				Timeout: gotgbot.DefaultTimeout,
				APIURL:  gotgbot.DefaultAPIURL,
			},
		},
	)
	if err != nil {
		return nil, err
	}

	// Create updater and dispatcher.
	updater := ext.NewUpdater(
		&ext.UpdaterOpts{
			Dispatcher: ext.NewDispatcher(
				&ext.DispatcherOpts{
					// If an error is returned by a handler, log it and continue going.
					Error: func(b *gotgbot.Bot, ctx *ext.Context, err error) ext.DispatcherAction {
						slog.Error(fmt.Sprintf("an error occurred while handling update: %s", err.Error()))
						return ext.DispatcherActionNoop
					},
					MaxRoutines: ext.DefaultMaxRoutines,
				},
			),
		},
	)
	dispatcher := updater.Dispatcher

	// Adding bot commands handlers
	dispatcher.AddHandler(handlers.NewCommand("start", start))
	dispatcher.AddHandler(handlers.NewCommand("help", start))
	dispatcher.AddHandler(handlers.NewCommand("img", imgHandler(fileDirectory, hook)))

	// Start receiving updates.
	err = updater.StartPolling(
		b,
		&ext.PollingOpts{
			DropPendingUpdates: true,
			GetUpdatesOpts: gotgbot.GetUpdatesOpts{
				Timeout: 9,
				RequestOpts: &gotgbot.RequestOpts{
					Timeout: time.Second * 10,
				},
			},
		},
	)
	if err != nil {
		return nil, err
	}

	slog.Info(fmt.Sprintf("%s on telegram has been started", b.User.Username))

	return b, nil
}
