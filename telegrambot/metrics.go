package telegrambot

import "github.com/prometheus/client_golang/prometheus"

// Metrics implements the Hook interface
type Metrics struct {
	errorNb  prometheus.Counter
	response prometheus.Histogram
}

func NewMetrics(reg prometheus.Registerer) *Metrics {
	m := &Metrics{
		errorNb: prometheus.NewCounter(
			prometheus.CounterOpts{
				Namespace: "ris",
				Subsystem: "telegram",
				Name:      "error_number",
				Help:      "number of errors while serving files",
			},
		),
		response: prometheus.NewHistogram(
			prometheus.HistogramOpts{
				Namespace: "ris",
				Subsystem: "telegram",
				Name:      "responses",
				Help:      "response time in milisecond",
			},
		),
	}

	reg.MustRegister(m.errorNb)
	reg.MustRegister(m.response)

	return m
}

func (w *Metrics) AddError(err error) {
	w.errorNb.Inc()
}

func (w *Metrics) Duration(duration int64) {
	w.response.Observe(float64(duration))
}
