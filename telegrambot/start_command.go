package telegrambot

import (
	"fmt"

	"github.com/PaulSonOfLars/gotgbot/v2"
	"github.com/PaulSonOfLars/gotgbot/v2/ext"
)

// start is a command to introduces the bot.
func start(b *gotgbot.Bot, ctx *ext.Context) error {
	_, err := ctx.EffectiveMessage.Reply(
		b,
		fmt.Sprintf("Hello, I'm @%s. I send random images. Use /img to get one", b.User.Username),
		&gotgbot.SendMessageOpts{
			ParseMode: "html",
		},
	)
	if err != nil {
		return fmt.Errorf("failed to send start message: %w", err)
	}

	return nil
}
