package webserver

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"gitlab.com/albandewilde/ris/randomfile"
)

// New return a http.Server configured to serve random files in the `fileDirectory` directory.
// It listen on the address `host`:`port`
func New(
	host string,
	port int,
	fileDirectory string,
	hook Hook,
) *http.Server {
	srv := http.Server{
		Addr: fmt.Sprintf("%s:%d", host, port),
		Handler: http.HandlerFunc(
			getServeFileHandler(fileDirectory, hook),
		),
	}

	// Start the server
	log.Printf("Serving started on http://%s\n", srv.Addr)
	go srv.ListenAndServe()

	return &srv
}

// getServerFileHandler return an handler to serve files in a configured directory
func getServeFileHandler(fileDirectory string, hook Hook) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		// Allow CORS request
		w.Header().Set("Access-Control-Allow-Origin", "*")

		content, err := randomfile.GetContent(fileDirectory)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Failed to get a random file"))
			hook.AddError(err)
			log.Println(fmt.Errorf("can't get random file: %w", err))
			return
		}

		// Serve the file
		w.Write(content)

		hook.Duration(time.Since(start).Milliseconds())
	}
}
