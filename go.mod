module gitlab.com/albandewilde/ris

go 1.20

require (
	github.com/PaulSonOfLars/gotgbot/v2 v2.0.0-rc.20
	github.com/bwmarrin/discordgo v0.27.0
	github.com/hajimehoshi/ebiten/v2 v2.6.2
	github.com/prometheus/client_golang v1.16.0
	golang.org/x/exp v0.0.0-20230728194245-b0cb94b80691
	golang.org/x/image v0.12.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/ebitengine/purego v0.5.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/jezek/xgb v1.1.0 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.4 // indirect
	github.com/prometheus/client_model v0.3.0 // indirect
	github.com/prometheus/common v0.42.0 // indirect
	github.com/prometheus/procfs v0.10.1 // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/exp/shiny v0.0.0-20231006140011-7918f672742d // indirect
	golang.org/x/mobile v0.0.0-20230922142353-e2f452493d57 // indirect
	golang.org/x/sync v0.3.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	google.golang.org/protobuf v1.30.0 // indirect
)
